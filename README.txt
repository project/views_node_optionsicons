README.txt
----------
Views Node Options Icons provides global icons for node options in views like: open, edit, delete, ... 
optionally overrideable by theme.
This is very helpful if you use those icons in several view on your Drupal page, 
because they can be added very quickly and consistently. Not a lot of modification needed.
Otherwise a module like views_customfield (Markup) might be an alternative, 
if you just need icons in very special cases.
If you need further icon support for other links, add an issue please. 
Icon selection / icon sets are currently not planned.

INSTALLATION
------------
1. Download and enable this module:
   Copy views_nodeoptionsicons directory to /sites/all/modules (for example).
2. Add the fields you wish in your views fields and configure them. 
You find them inside the "Node"-Group. You may enter a "title" and an "alt" attribute value for each icon. 

CONFIGURATION
------------
No separate configuration. Each field may be configured separately in the views settings.
If you wish to override an icon within your theme, just add a new folder: 
"views_nodeoptionsicons/icons/" in your theme folder. 
The replacing icons must be named like the original. Don't forget to clear your cache after that!

AUTHOR / MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Thomas Frobieter (http://blog.frobieter.de)