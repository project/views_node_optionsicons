<?php

/**
 * @file
 * Contains the views 2 configuration for the views node links icons.
 * Defines the handlers and the selectable fields.
 */

/**
 * Implementation of hook_views_handlers.
 */
function views_nodeoptionsicons_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_nodeoptionsicons') . '/handlers',
    ),
    'handlers' => array(
      // field handlers
      'views_nodeoptionsicons_handler_field_view_node_icon' => array(
        'parent' => 'views_handler_field_node',
      ),
      'views_nodeoptionsicons_handler_field_node_link_edit_icon' => array(
        'parent' => 'views_handler_field_node_link_edit',
      ),
      'views_nodeoptionsicons_handler_field_node_link_delete_icon' => array(
        'parent' => 'views_handler_field_node_link_delete',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data.
 */
function views_nodeoptionsicons_views_data() {
  $data['node']['view_node_icon'] = array(
    'field' => array(
      'title' => t('Link (with icon)'),
      'help' => t('Provide a simple icon link to the node.'),
      'handler' => 'views_nodeoptionsicons_handler_field_view_node_icon',
      'click sortable' => FALSE,
    ),
  );

  $data['node']['edit_node_icon'] = array(
    'field' => array(
      'title' => t('Edit link (with icon)'),
      'help' => t('Provide a simple icon link to edit the node.'),
      'handler' => 'views_nodeoptionsicons_handler_field_node_link_edit_icon',
      'click sortable' => FALSE,
    ),
  );

  $data['node']['delete_node_icon'] = array(
    'field' => array(
      'title' => t('Delete link (with icon)'),
      'help' => t('Provide a simple icon link to delete the node.'),
      'handler' => 'views_nodeoptionsicons_handler_field_node_link_delete_icon',
      'click sortable' => FALSE,
    ),
  );
  
  return $data;
}