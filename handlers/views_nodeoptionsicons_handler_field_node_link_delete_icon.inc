<?php

/**
 * @file
 * Contains the configuration for the views node link icon field: delete node
 */

/**
 * Field handler to present a link with icon to delete a node.
 */
class views_nodeoptionsicons_handler_field_node_link_delete_icon extends views_handler_field_node_link_delete {
  /**
   * The icons filename including suffix.
   */
  protected $icon_name = 'node_link_delete.png';

  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['format'] = array('table' => 'node_revisions', 'field' => 'format');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['title'] = array('default' => t('Delete'), 'translatable' => TRUE);
    $options['alt'] = array('default' => t('Delete icon'), 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['text']);

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title of the icon'),
      '#default_value' => $this->options['title'],
    );

    $form['alt'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt-Text of the icon'),
      '#default_value' => $this->options['alt'],
    );

    $form['icon_information'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">' . t('You may override the icon globally in your theme by adding: %icon_path', array('%icon_path' => 'YOUR_THEME/' . 'views_nodeoptionsicons' . '/' . views_nodeoptionsicons_get_icon_folder() . '/' . $this->icon_name)) . '</div>',
    );
  }

  function render($values) {
    // ensure user has access to edit this node.
    $node = new stdClass();
    $node->nid = $values->{$this->aliases['nid']};
    $node->uid = $values->{$this->aliases['uid']};
    $node->type = $values->{$this->aliases['type']};
    $node->format = $values->{$this->aliases['format']};
    $node->status = 1; // unpublished nodes ignore access control
    if (!node_access('delete', $node)) {
      return;
    }

    // Additional fields.
    $title = !empty($this->options['title']) ? $this->options['title'] : t('Delete');
    $alt = !empty($this->options['alt']) ? $this->options['alt'] : t('Delete icon');

    // Get Icon from theme override or module default.
    $image_path = views_nodeoptionsicons_get_icon_path($this->icon_name);

    // Create image HTML.
    $image = theme('image', $image_path, $alt, $title);

    return l($image, "node/$node->nid/delete", array('query' => drupal_get_destination(), 'html' => TRUE));
  }
}

