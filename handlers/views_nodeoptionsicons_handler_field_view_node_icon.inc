<?php

/**
 * @file
 * Contains the configuration for the views node link icon field: open node
 */

/**
 * Field handler to present a link with icon to the node.
 */
class views_nodeoptionsicons_handler_field_view_node_icon extends views_handler_field_node {
  /**
   * The icons filename including suffix.
   */
  protected $icon_name = 'view_node.png';

  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['title'] = array('default' => t('Open'), 'translatable' => TRUE);
    $options['alt'] = array('default' => t('Open icon'), 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title of the icon'),
      '#default_value' => $this->options['title'],
    );

    $form['alt'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt-Text of the icon'),
      '#default_value' => $this->options['alt'],
    );

    $form['icon_information'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">' . t('You may override the icon globally in your theme by adding: %icon_path', array('%icon_path' => 'YOUR_THEME/' . 'views_nodeoptionsicons' . '/' . views_nodeoptionsicons_get_icon_folder() . '/' . $this->icon_name)) . '</div>',
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $nid = $values->{$this->aliases['nid']};

    // Additional fields.
    $title = !empty($this->options['title']) ? $this->options['title'] : t('Open');
    $alt = !empty($this->options['alt']) ? $this->options['alt'] : t('Open icon');

    // Get Icon from theme override or module default.
    $image_path = views_nodeoptionsicons_get_icon_path($this->icon_name);

    // Create image HTML.
    $image = theme('image', $image_path, $alt, $title);

    // Return linked image!
    return l($image, "node/$nid", array('html' => TRUE));
  }
}